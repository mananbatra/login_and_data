/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)


Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/


function login(user, val) {
    if(val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1, 
            name: "Test",
        },
        {
            id: 2, 
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    // use promises and fs to save activity in some file
}

/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/

const fs = require('fs');
const { resolve } = require('path');

function writeNewFile(filePath,data) {                           // function for creation of file

    return new Promise((resolve, reject) => {
        fs.writeFile(filePath, JSON.stringify(data), (error) => {
            if (error) {
                reject("error happened while creation",error);
            }
            else {
                resolve(`file created SuccessFully`);
            }
        });
    });
}

function deleteFile(fileName) {                         // function for deletion of file
    
    return fs.unlink(fileName, (err) => {
        if (err) {
            reject("error happened while deletion",err)
        } else {
            resolve("File Successfully Deleted")
        }
    });
    
}

function delayExectution(ms){                           // function for delay
    return new Promise((resolve) => setTimeout(resolve, ms*1000));
  };

function creationAndDeletion()
{
    const file1="file1.txt";
    const file2="file2.txt";

    const writeFile1=writeNewFile(file1,"hello everyone this is file1");
    const writeFile2=writeNewFile(file2,"hello everyone this is file2");

    Promise.allSettled([writeFile1,writeFile2])                // create both files simultaniously
        .then(() =>{
           return delayExectution(2)
        })
        .then(() => {
           return deleteFile(file1);
        })
        .then(() => {
           return deleteFile(file2);
        })
        .then(() => {
           return console.log('All files have been created and deleted successfully.');
          })
          .catch((error) => {
            console.error('An error occurred:', error);
          });
}

creationAndDeletion();




